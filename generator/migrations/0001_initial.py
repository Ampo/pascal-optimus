# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Composant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('id_composant', models.PositiveIntegerField()),
                ('texte_composant', models.CharField(max_length=50)),
                ('type_composant', models.CharField(default=b'CO', max_length=2, choices=[(b'PR', b'Prefixe'), (b'SU', b'Suffixe'), (b'CO', b'Complement')])),
            ],
        ),
        migrations.CreateModel(
            name='Effet',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('texte_effet', models.CharField(max_length=250)),
                ('genre_effet', models.CharField(default=b'PO', max_length=2, choices=[(b'PO', b'Positif'), (b'IN', b'Indesirable'), (b'CA', b'Cancerigene'), (b'AL', b'Allergene'), (b'MO', b'Mortel')])),
            ],
        ),
        migrations.CreateModel(
            name='Forme_Composant',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('texte_forme_composant', models.CharField(max_length=20)),
                ('label_forme_composant', models.CharField(max_length=2)),
            ],
        ),
        migrations.AddField(
            model_name='composant',
            name='effets_composant',
            field=models.ManyToManyField(to='generator.Effet'),
        ),
        migrations.AddField(
            model_name='composant',
            name='forme_composant',
            field=models.ForeignKey(to='generator.Forme_Composant'),
        ),
    ]
