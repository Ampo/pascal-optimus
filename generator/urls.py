from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.main_page, name='main_page'),
    url(r'^generate/$', views.generate, name="generate"),
    url(r'^(?P<prefixe>[a-zA-Z]+)/(?P<suffixe>[a-zA-Z]+)/$',
        views.detail, name='detail'),
    url(r'^(?P<prefixe>[a-zA-Z]+)/(?P<complement1>[a-zA-Z]+)/(?P<suffixe>[a-zA-Z]+)/$',
        views.detail, name='detail'),
    url(r'^(?P<prefixe>[a-zA-Z]+)/(?P<complement1>[a-zA-Z]+)/(?P<complement2>[a-zA-Z]+)/(?P<suffixe>[a-zA-Z]+)/$',
        views.detail, name='detail'),
    url(r'^(?P<prefixe>[a-zA-Z]+)/(?P<complement1>[a-zA-Z]+)/(?P<complement2>[a-zA-Z]+)/(?P<complement3>[a-zA-Z]+)/(?P<suffixe>[a-zA-Z]+)/$',
        views.detail, name='detail'),
    url(r'^(?P<seed>[0-9]+)/$', views.vote_up, name='vote_up'),
    url(r'^top/$', views.top, name='top'),
    url(r'^compose/$', views.compose, name='compose'),
    url(r'^suggest/$', views.suggest, name='suggest'),
    url(r'^thanks/$', views.thanks, name='thanks'),
    url(r'^.*$', views.error404, name='error404'),
]
