# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('generator', '0002_remove_composant_id_composant'),
    ]

    operations = [
        migrations.CreateModel(
            name='Statistique',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('texte_statistique', models.CharField(max_length=30)),
            ],
        ),
    ]
