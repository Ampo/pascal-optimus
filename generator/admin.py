from django.contrib import admin
from .models import Composant, Effet, Forme_Composant, Statistique, Vote, Vote_instance, Su_Effet, Su_Composant, Su_Statistique

# Register your models here.
admin.site.register(Composant)
admin.site.register(Effet)
admin.site.register(Forme_Composant)
admin.site.register(Statistique)
admin.site.register(Vote)
admin.site.register(Vote_instance)
admin.site.register(Su_Effet)
admin.site.register(Su_Statistique)
admin.site.register(Su_Composant)
