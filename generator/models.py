from django.db import models
from django.db.models import Max
from datetime import datetime
from sympy import sieve


def primes(n):
    return list(sieve.primerange(1, n))


class Composant(models.Model):
    PREFIXE = "PR"
    SUFFIXE = "SU"
    COMPLEMENT = "CO"
    TYPE_CHOICES = (
        (PREFIXE, "Prefixe"),
        (SUFFIXE, "Suffixe"),
        (COMPLEMENT, "Complement"),
    )
    texte_composant = models.CharField(max_length=50)
    type_composant = models.CharField(choices=TYPE_CHOICES, max_length=2,
                                      default=COMPLEMENT)
    forme_composant = models.ForeignKey('Forme_Composant')
    effets_composant = models.ManyToManyField('Effet')

    @property
    def id_composant(self):
        return primes(2000000)[self.pk-1]

    def __unicode__(self):
        return str(self.id_composant)+" "+self.texte_composant

    def __str__(self):
        return str(self.id_composant)+" "+self.texte_composant


class Effet(models.Model):
    POSITIF = "PO"
    INDESIRABLE = "IN"
    CANCERIGENE = "CA"
    ALLERGENE = "AL"
    MORTEL = "MO"
    GENRE_CHOICES = (
        (POSITIF, "Positif"),
        (INDESIRABLE, "Indesirable"),
        (CANCERIGENE, "Cancerigene"),
        (ALLERGENE, "Allergene"),
        (MORTEL, "Mortel"),
    )
    texte_effet = models.CharField(max_length=250)
    genre_effet = models.CharField(choices=GENRE_CHOICES, max_length=2,
                                   default=POSITIF)

    def __unicode__(self):
        return self.genre_effet+" "+self.texte_effet

    def __str__(self):
        return self.genre_effet+" "+self.texte_effet


class Forme_Composant(models.Model):
    texte_forme_composant = models.CharField(max_length=20)
    label_forme_composant = models.CharField(max_length=2)

    def __unicode__(self):
        return self.texte_forme_composant

    def __str__(self):
        return self.texte_forme_composant


class Statistique(models.Model):
    texte_statistique = models.CharField(max_length=30)

    def __unicode__(self):
        return self.texte_statistique

    def __str__(self):
        return self.texte_statistique


class Vote(models.Model):
    seed_medicament = models.IntegerField()
    votes = models.IntegerField()


class Vote_instance(models.Model):
    seed_medicament = models.IntegerField()
    ip_vote = models.CharField(max_length=64)


class Su_Effet(models.Model):
    POSITIF = "PO"
    INDESIRABLE = "IN"
    CANCERIGENE = "CA"
    ALLERGENE = "AL"
    MORTEL = "MO"
    GENRE_CHOICES = (
        (POSITIF, "Positif"),
        (INDESIRABLE, "Indesirable"),
        (CANCERIGENE, "Cancerigene"),
        (ALLERGENE, "Allergene"),
        (MORTEL, "Mortel"),
    )
    texte_effet = models.CharField(max_length=250)
    genre_effet = models.CharField(choices=GENRE_CHOICES, max_length=2,
                                   default=POSITIF)

    def __unicode__(self):
        return self.genre_effet+" "+self.texte_effet

    def __str__(self):
        return self.genre_effet+" "+self.texte_effet
    suggest_date = models.DateField(default=datetime.now())


class Su_Statistique(models.Model):
    texte_statistique = models.CharField(max_length=30)

    def __unicode__(self):
        return self.texte_statistique

    def __str__(self):
        return self.texte_statistique
    suggest_date = models.DateField(default=datetime.now())


class Su_Composant(models.Model):
    PREFIXE = "PR"
    SUFFIXE = "SU"
    COMPLEMENT = "CO"
    TYPE_CHOICES = (
        (PREFIXE, "Prefixe"),
        (SUFFIXE, "Suffixe"),
        (COMPLEMENT, "Complement"),
    )
    texte_composant = models.CharField(max_length=50)
    type_composant = models.CharField(choices=TYPE_CHOICES, max_length=2,
                                      default=COMPLEMENT)
    forme_composant = models.ForeignKey('Forme_Composant')
    effets_composant = models.ManyToManyField('Effet')

    @property
    def id_composant(self):
        return primes(2000000)[self.pk-1]


    def __unicode__(self):
        return str(self.id_composant)+" "+self.texte_composant

    def __str__(self):
        return str(self.id_composant)+" "+self.texte_composant

    suggest_date = models.DateField(default=datetime.now())
