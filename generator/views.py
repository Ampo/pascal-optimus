from django.shortcuts import render, redirect
from .models import Composant, Effet, Forme_Composant, Statistique, Vote
from .models import Vote_instance, Su_Effet, Su_Composant, Su_Statistique
from datetime import datetime
import random
import unicodedata

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect


def strip_accents(s):
    return ''.join(c for c in unicodedata.normalize('NFD', s)
                   if unicodedata.category(c) != 'Mn')


# Create your views here.
def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def primes(n):
    primfac = []
    d = 2
    while d*d <= n:
        while (n % d) == 0:
            primfac.append(d)  # supposing you want multiple factors repeated
            n //= d
        d += 1
    if n > 1:
        primfac.append(n)
    return primfac

from sympy import sieve


def primes_id(n):
    return list(sieve.primerange(1, n))


def main_page(request):
    compos = Composant.objects.all()[::-1][:5]
    effets = Effet.objects.all()[::-1][:5]
    su_num = len(Su_Effet.objects.all()) + len(Su_Composant.objects.all()) + len(Su_Statistique.objects.all())
    nb_effects = len(Effet.objects.all())
    nb_compo = len(Composant.objects.all())
    nb_stat = len(Statistique.objects.all())
    nb_pref = len(Composant.objects.filter(type_composant="PR"))
    nb_suf = len(Composant.objects.filter(type_composant="SU"))
    nb_complement = len(Composant.objects.filter(type_composant="CO"))
    nb_combinaisons = nb_pref * nb_suf
    nb_combinaisons += nb_combinaisons * nb_complement + nb_combinaisons*nb_complement**2 + nb_combinaisons*nb_complement**3
    return render(request, 'generator/index.html', {'compos': compos,
                                                    'effets': effets,
                                                    'su_num': su_num,
                                                    'nb_effects': nb_effects,
                                                    'nb_compo': nb_compo,
                                                    'nb_stat': nb_stat,
                                                    'nb_combinaisons': nb_combinaisons})


def top(request):
    votes = Vote.objects.order_by('-votes')[:25]
    medocs = []
    primes_seive = primes_id(200000)
    for vote in votes:
        prime_decomp = primes(vote.seed_medicament)
        nom_medoc = []
        string_medoc = ""
        path_medoc = ""
        for prime in prime_decomp:
            k = 1
            for prime_s in primes_seive:
                if prime_s == prime:
                    break
                k += 1
            nom_medoc.append(Composant.objects.get(pk=k))
        for nom in nom_medoc:
            if nom.type_composant == "PR":
                string_medoc += nom.texte_composant
                path_medoc += nom.texte_composant
        complements = [x for x in nom_medoc if x.type_composant == "CO"]
        for complement in complements:
            string_medoc += complement.texte_composant
            path_medoc += "/" + complement.texte_composant
        string_medoc += [x for x in nom_medoc if x.type_composant == "SU"][0].texte_composant
        path_medoc += "/" + [x for x in nom_medoc if x.type_composant == "SU"][0].texte_composant + "/"
        medocs.append({'nom': string_medoc,
                       'votes': vote.votes,
                       'path': path_medoc})

    first = medocs[0]
    medocs = medocs[1:]
    second = medocs[0]
    medocs = medocs[1:]
    third = medocs[0]
    medocs = medocs[1:]

    return render(request, 'generator/top.html',
                  {'medocs': medocs, 'first': first,
                   'second': second, 'third': third})


def compose(request):
    prefixes = Composant.objects.filter(type_composant='PR').order_by('texte_composant').all()
    suffixes = Composant.objects.filter(type_composant='SU').order_by('texte_composant').all()
    compos = Composant.objects.filter(type_composant='CO').order_by('texte_composant').all()
    return render(request, 'generator/compose.html',
                  {'prefixes': prefixes,
                   'suffixes': suffixes,
                   'compos': compos})

def suggest(request):
    formes = Forme_Composant.objects.all()
    effets = Effet.objects.all()
    return render(request, 'generator/suggest.html',
                  {'genres': Effet.GENRE_CHOICES, 'formes': formes,
                   'effets': effets})


def generate(request):
    prefixe = Composant.objects.filter(type_composant="PR").order_by('?').first()
    suffixe = Composant.objects.filter(type_composant="SU").order_by('?').first()
    nb_composants = random.randint(0, 3)
    composants = []
    for num in range(nb_composants):
        composants.append(random.choice(Composant.objects.filter(type_composant="CO").all()))
    # Generation de l'url
    url = prefixe.texte_composant + "/"
    for comp in composants:
        url += comp.texte_composant + "/"
    url += suffixe.texte_composant + "/"
    return redirect('../'+url)


def error404(request):
    return render(request, 'generator/404.html')


def vote_up(request, seed):
    ip = get_client_ip(request)
    try:
        Vote_instance.objects.get(ip_vote=ip, seed_medicament=seed)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))
    except:
        Vote_instance.objects.create(ip_vote=ip, seed_medicament=seed)
        vote_val = Vote.objects.get(seed_medicament=seed).votes
        vote = Vote.objects.filter(seed_medicament=seed).update(votes=vote_val+1)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER', '/'))


def detail(request, prefixe, suffixe, complement1=None, complement2=None,
           complement3=None):
    try:
        prefixe = Composant.objects.get(texte_composant=prefixe)
        suffixe = Composant.objects.get(texte_composant=suffixe)
    except:
        return render(request, 'generator/404.html')
    effets = []
    support = []
    support.append(prefixe.forme_composant)
    support.append(suffixe.forme_composant)
    for e in prefixe.effets_composant.all():
        if e not in effets:
            effets.append(e)
    for e in suffixe.effets_composant.all():
        if e not in effets:
            effets.append(e)
    seed = prefixe.id_composant * suffixe.id_composant
    nom_medicament = prefixe.texte_composant
    # Gestion des eventuels complements
    nb_complement = 0
    if complement1 != None:
        nb_complement += 1
        complement1 = Composant.objects.get(texte_composant=complement1)
        seed *= complement1.id_composant
        nom_medicament += complement1.texte_composant
        for e in complement1.effets_composant.all():
            if e not in effets:
                effets.append(e)
        support.append(complement1.forme_composant)

    if complement2 != None:
        nb_complement += 1
        complement2 = Composant.objects.get(texte_composant=complement2)
        seed *= complement2.id_composant
        nom_medicament += complement2.texte_composant
        for e in complement2.effets_composant.all():
            if e not in effets:
                effets.append(e)
        support.append(complement2.forme_composant)

    if complement3 != None:
        nb_complement += 1
        complement3 = Composant.objects.get(texte_composant=complement3)
        seed *= complement3.id_composant
        nom_medicament += complement3.texte_composant
        for e in complement3.effets_composant.all():
            if e not in effets:
                effets.append(e)
        support.append(complement3.forme_composant)

    random.seed(seed)
    nom_medicament += suffixe.texte_composant
    id_support = seed % (nb_complement+2)

    stats = Statistique.objects.all()
    stats = list(stats)
    random.shuffle(stats)
    stats = stats[7:]

    stats_num = []
    for num in range(0, 8):
        stats_num.append(random.randint(0, 10))

    if prefixe.type_composant != "PR":
        return render(request, 'generator/404.html')
    if suffixe.type_composant != "SU":
        return render(request, 'generator/404.html')

    effets_PO = [n for n in effets if n.genre_effet == "PO"]
    effets_IN = [n for n in effets if n.genre_effet == "IN"]
    effets_CA = [n for n in effets if n.genre_effet == "CA"]
    effets_AL = [n for n in effets if n.genre_effet == "AL"]
    effets_MO = [n for n in effets if n.genre_effet == "MO"]

    try:
        support_img = Forme_Composant.objects.get(texte_forme_composant=support[id_support])
    except:
        support_img = Forme_Composant.objects.all()[0]

    support_img = strip_accents(support_img.texte_forme_composant.lower())

    if support_img == "injection":
        support_img = "vaccin"


    try:
        vote = Vote.objects.get(seed_medicament=seed)
    except:
        Vote.objects.create(seed_medicament=seed, votes=0)
    vote = Vote.objects.get(seed_medicament=seed)
    vote_val = vote.votes

    return render(request, 'generator/detail.html',
                  {'nom_medicament': nom_medicament, 'seed': seed,
                   'effets_PO': effets_PO, 'effets_IN': effets_IN,
                   'effets_CA': effets_CA, 'effets_AL': effets_AL,
                   'effets_MO': effets_MO,
                   'support': support[id_support], 'stats': stats,
                   'stats_num': stats_num, 'vote': vote_val,
                   'support_img': support_img,
                   })


def thanks(request):
    if request.method == "POST":
        infos = dict(request.POST)
        if infos['form'][0] == u'effet':
            # Manage effect suggest form

            effet_text = infos['effect_text'][0]
            effet_genre = infos['effect_genre'][0]

            Su_Effet.objects.create(texte_effet=effet_text, genre_effet=effet_genre, suggest_date=datetime.now())

            return render(request, 'generator/thanks.html',
                          {'mode': 'effet', 'effet_text': effet_text, 'effet_genre': effet_genre})
        elif infos['form'][0] == u'composant':
            # Manage composant suggest form

            composant_effects = list(map(int, infos['filters'][0].split(',')))

            composant_form = infos['composant_form'][0]
            composant_text = infos['composant_text'][0]
            composant_type = infos['type'][0]

            composant = Su_Composant()
            composant.suggest_date = datetime.now()
            composant.texte_composant = composant_text
            composant.type_composant = composant_type
            composant.forme_composant = Forme_Composant.objects.get(pk=str(composant_form))

            composant.save()
            text_effets = list()

            for k in composant_effects:
                composant.effets_composant.add(Effet.objects.get(id=int(k)))
                text_effets.append(Effet.objects.get(id=int(k)).texte_effet)

            composant.save()

            return render(request, 'generator/thanks.html',
                          {'mode': 'composant', 'text_effets': text_effets, 'texte_composant': composant.texte_composant, 'type_composant': composant.type_composant, 'forme_composant': composant.forme_composant})
        elif infos['form'][0] == u'statistique':
            # Manage statistique suggest form

            Su_Statistique.objects.create(texte_statistique=infos['first-name'][0], suggest_date=datetime.now())

            return render(request, 'generator/thanks.html',
                          {'mode': 'statistique', 'texte_statistique': infos['first-name'][0]})

    return render(request, 'generator/404.html')
