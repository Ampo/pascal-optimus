# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('generator', '0004_vote'),
    ]

    operations = [
        migrations.CreateModel(
            name='Vote_instance',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('seed_medicament', models.IntegerField()),
                ('ip_vote', models.CharField(max_length=64)),
            ],
        ),
    ]
